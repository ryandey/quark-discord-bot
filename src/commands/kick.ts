import * as Discord from "discord.js";
import { IBotCommand } from "../api";

export default class kick implements IBotCommand {

    private readonly _command = "kick";

    help(): string {
        return "Kicks the mentioned user.";
    }

    isThisCommand(command: string): boolean {
        return command === this._command;
    }

    runCommand(args: string[], msgObject: Discord.Message, client: Discord.Client): void {

        let mentionedUser = msgObject.mentions.users.first(); // If mentioned multiple people, take only first one
        let suppliedReason = args.slice(1).join(" ") || ""; // Gives us everything after the command, if nothing, empty
        let kickLog = `${msgObject.author.username}: ${suppliedReason}`;

        if (!msgObject.member.hasPermission("ADMINISTRATOR")) {
            msgObject.channel.send(`Nice try ${msgObject.author.username}, but you don't have permission for that command.`);
            return;
        }

        if (!mentionedUser) {
            msgObject.channel.send(`Sorry ${msgObject.author.username}, I couldn't find a user to kick with that name.`);
            return;
        }

        msgObject.delete(0);

        msgObject.guild.member(mentionedUser).kick(kickLog)
            .then(console.log)
            .catch(console.error)
    }
}