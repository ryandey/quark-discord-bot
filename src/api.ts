import * as Discord from "discord.js";

export interface IBotCommand {
    // .help - loop through all commands to list descriptions for all commands
    help(): string;
    isThisCommand(command: string): boolean;
    runCommand(args: string[], msgObject: Discord.Message, client: Discord.Client): void;
}