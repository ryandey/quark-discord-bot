import * as Discord from "discord.js";
import * as ConfigFile from "./config";
import { IBotCommand } from "./api";

// Allows us to use all code related to Discord Clients
const client: Discord.Client = new Discord.Client();

let commands: IBotCommand[] = [];

loadCommands(`${__dirname}/commands`)

client.on("ready", () => {

    // Lets us know when the bot is online/ready
    console.log("Ready to work!");
})

// When a message is received, do this
client.on("message", msg => {

    // Ignore message if sent by the bot
    if (msg.author.bot) { return; }

    // Ignore messages that don't start with defined prefix
    if (!msg.content.startsWith(ConfigFile.config.prefix)) { return; }

    // Handle the command
    handleCommand(msg);
})

async function handleCommand(msg: Discord.Message) {

    // Split string into command and all args. Replace prefix with nothing
    let command = msg.content.split(" ")[0].replace(ConfigFile.config.prefix, "");
    let args = msg.content.split(" ").slice(1);

    // Loop through all loaded commands
    for (const commandClass of commands) {
        // Attempt to execute code but ready in case of possible error
        try {
            // Check if our command class is the correct one
            if (!commandClass.isThisCommand(command)) {
                continue; // Go to next iteration of loop if not correct command
            }
            // Pause execution whilst we run command's code
            await commandClass.runCommand(args, msg, client);
        }
        catch (exception) {
            // If there is an error, then log exception to console
            console.log(exception);
        }
    }
}

function loadCommands(commandsPath: string) {

    // Exit if there are no commands
    if (!ConfigFile.config || (ConfigFile.config.commands as string[]).length === 0) { return; }

    // Loops through all of the commands in our config file
    for (const commandName of ConfigFile.config.commands as string[]) {

        // Store reference to command file
        const commandsClass = require(`${commandsPath}/${commandName}`).default;
        const command = new commandsClass() as IBotCommand;
        commands.push(command); // Add to commands list
    }
}

// Make the bot log in to Discord using token from config.ts
client.login(ConfigFile.config.token);